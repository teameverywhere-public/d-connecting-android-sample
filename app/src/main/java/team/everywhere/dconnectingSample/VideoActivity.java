package team.everywhere.dconnectingSample;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.multidex.MultiDex;

import org.webrtc.EglRenderer;
import org.webrtc.SurfaceViewRenderer;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import everywhere.team.DeepConnecting.DeepConnectingSDK;
import everywhere.team.DeepConnecting.SignallingClient;


public class VideoActivity extends Activity {
    private static final String TAG = "VideoActivity";
    private int viewHeight, viewWidth;
    SurfaceViewRenderer svUser1, svUser2;
    public boolean videoSwitched = false;
    public static VideoActivity mActivity;
    public static ProgressBar pbMain;
    private String stAuthSid, stAuthToken, stId, stRoomName;
    public static int pos = 0;
    Button btnConnect, btnCall, btnCapture, btnVideo, btnHangUp, btnSwitch, btnSendMessage;
    Spinner spinner;
    private ArrayAdapter<String> mArrayAdapter;
    public static boolean cameraChanged = false;



    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        binding = DataBindingUtil.setContentView(this, R.layout.activity_video);
        setContentView(R.layout.activity_video);

        //init signaling client
        SignallingClient.getInstance(this);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        viewHeight = displayMetrics.heightPixels;
        viewWidth = displayMetrics.widthPixels;

        svUser1 = (SurfaceViewRenderer) findViewById(R.id.svUser1);
        svUser2 =(SurfaceViewRenderer)  findViewById(R.id.svUser2);
        btnConnect =(Button) findViewById(R.id.btnConnect);
        btnCall =(Button) findViewById(R.id.btnCall);
        btnCapture =(Button) findViewById(R.id.btnCapture);
        btnVideo =(Button) findViewById(R.id.btnVideo);
        btnHangUp =(Button) findViewById(R.id.btnHangUp);
        btnSwitch =(Button) findViewById(R.id.btnSwitch);
        btnSendMessage =(Button) findViewById(R.id.btnSendMessage);
        spinner = (Spinner) findViewById(R.id.spinner);
        pbMain =(ProgressBar) findViewById(R.id.pbMain);

        ViewGroup.LayoutParams params = svUser1.getLayoutParams();
        params.height = viewHeight / 3;
        params.width = viewWidth / 3;
        svUser1.setLayoutParams(params);
        svUser1.setZOrderOnTop(true);

        mActivity = this;
        showMainProgressBar(false);

        ImageView ivCaptured = (ImageView) findViewById(R.id.ivCaptured);
        Button btnCapture =(Button) findViewById(R.id.btnCapture);
        btnCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //capture remote image
                svUser2.addFrameListener(new EglRenderer.FrameListener() {
                    @Override
                    public void onFrame(Bitmap bitmap) {
                        Log.d(TAG, "onFrame: captured bitmap width : "+bitmap.getWidth());
                        runOnUiThread(() -> {
                            ivCaptured.setImageBitmap(bitmap);
                            ivCaptured.setVisibility(View.VISIBLE);
                        });


                    }
                }, 1.0f);

            }
        });

        //close captured image view
        ivCaptured.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivCaptured.setVisibility(View.GONE);
            }
        });



        Button btnSwitchLocalBig =(Button) findViewById(R.id.btnSwitchLocalBig);
        btnSwitchLocalBig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                runOnUiThread(() -> {
                    DeepConnectingSDK.getInstance(getApplicationContext()).switchLocalRemote(videoSwitched);
                    videoSwitched = !videoSwitched;
                });

            }
        });

        //connect
        Button btnConnect = (Button) findViewById(R.id.btnConnect);
        btnConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eventConnect();
            }
        });

        // call after connect
        Button btnCall = (Button) findViewById(R.id.btnCall);
        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eventCall();
            }
        });

        //show video rendered surfaceview
        btnVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eventShowVideo();
            }
        });

        //send message using datachannel
        btnSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = "hello stranger";
                Charset charset = Charset.forName("UTF-8");
                ByteBuffer buffer = ByteBuffer.wrap(msg.getBytes(charset));
                DeepConnectingSDK.getInstance(getApplicationContext()).sendMessageToDatachnnal(buffer);
            }
        });

        //hang up
        Button btnHangUp = (Button) findViewById(R.id.btnHangUp);
        btnHangUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eventHangUp();
            }
        });


        //finish
        View exFinish = findViewById(R.id.exFinish);
        exFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eventFinish();
            }
        });

    }


    public void eventFinish() {

        finish();
    }

    public  void finishVideoView(){
        finish();
    }



    public void eventConnect() {

        showMainProgressBar(true);
        btnConnect.setEnabled(false);

        stAuthSid = "gLfvFGpz19aAtUGv05NUXDges7s7OUd1kSElqmhhU";
            stAuthToken = "LfZ32s1TqJNUJZZh5VKCZMwPcZyumPWpEfdcHD8ngdj8wOfnzJVEBj2hByVPSK";
        stId = "ddsssssdddda";
        stRoomName = "TestRoom";
        String stCountry = "SouthKorea";


        SignallingClient.getInstance(getApplicationContext()).SetTokenCallback(new SignallingClient.TokenInterface() {
            @Override
            public void onTokenGenerated() {
                Log.d(TAG, "onTokenGenerated: ");
                DeepConnectingSDK.getInstance(getApplicationContext()).setDeepConnectingObserver(new DeepConnectingSDK.DeepConnectingObserver() {
                    @Override
                    public void signalServerConnencted() {
                        // On Successfully Connected
                        Log.d(TAG, "signalServerConnencted: called");
                        runOnUiThread(() -> {
                            Toast.makeText(VideoActivity.this, "Connected Successfully", Toast.LENGTH_SHORT).show();
                            int videoWidth = 640;
                            int videoHeight = 360;
                            boolean hardwareEncoding = false;
                            DeepConnectingSDK.getInstance(getApplicationContext()).setVideo(videoWidth, videoHeight, hardwareEncoding);
                            // 위코드는 실행하지 않으면 default 값으로 들어 갑니다. 640/360/true
                            DeepConnectingSDK.getInstance(getApplicationContext()).switchCamera(btnSwitch);

                            showMainProgressBar(false);

                            btnCall.setEnabled(true);
                        });
                    }

                    @Override
                    public void connectionDisconnected() {
                        Log.d(TAG, "connectionDisconnected: ");
                        runOnUiThread(() ->  btnHangUp.performClick());
//

                    }

                    @Override
                    public void connected() {
                        Log.d(TAG, "connected: called");
                        runOnUiThread(() -> {
                            btnHangUp.setVisibility(View.VISIBLE);
                            btnVideo.setVisibility(View.VISIBLE);

                            showMainProgressBar(false);
                            if (pos != 0) {
                                DeepConnectingSDK.getInstance(getApplicationContext()).selectCamera(pos);
                            }
                            DeepConnectingSDK.getInstance(getApplicationContext()).onToggleMic(false);
                            DeepConnectingSDK.getInstance(getApplicationContext()).stopAudioManager();

                        });
                    }

                    @Override
                    public void dataChannelOppened() {
                        runOnUiThread(() -> {
                            btnSendMessage.setVisibility(View.VISIBLE);

                        });

                    }

                    @Override
                    public void onSocketDisconnected() {
                        Log.d(TAG, "onSockectDisconnected");
                    }

                    @Override
                    public void onRoomJoined() {
                        //called when user joined room by event connect;
                        Log.d(TAG, "onRoomJoined: ");

                    }

                    @Override
                    public void onHangupFinished() {
                        Log.d(TAG, "onHangupFinished: ");
                        //When hangup finish, close activity
                        finish();
                    }
                    //
                    @Override
                    public void onMessageFromDatachannel(ByteBuffer buffer) {
                        ByteBuffer data = buffer;
                        final byte[] bytes = new byte[data.capacity()];
                        data.get(bytes);
                        String strData = new String(bytes, Charset.forName("UTF-8"));
                        Log.d(TAG, "Got msg: " + strData);
                        runOnUiThread(() -> {

                            Toast.makeText(getApplicationContext(), "Get Message : "+strData,Toast.LENGTH_LONG).show();
                        });

                    }


                    @Override
                    public void didReceiveRemoteSDP() {
                        Log.d(TAG, "didReceiveRemoteSDP: called");
                    }

                    @Override
                    public void didReceiveCandidate() {
                        // On Called;
                        Log.d(TAG, "didReceiveCandidate: called");
                    }

                    @Override
                    public void didDiscoverLocalCandidate() {
                        Log.d(TAG, "didDiscoverLocalCandidate: called");
                    }
                });
            }
        });
        SignallingClient.instance.generateToken(
                stAuthSid,
                stAuthToken,
                stId,
                stRoomName,
                stCountry
        );
    }

    public void eventCall() {
        showMainProgressBar(true);
        DeepConnectingSDK.getInstance(getApplicationContext()).onTryToStart();
    }

    public void eventHangUp() {

        Log.d(TAG, "eventHangUp");

        //close peer connection
        //don't call hangup multiple times on UI logic
        DeepConnectingSDK.getInstance(getApplicationContext()).hangup();

    }

    public void eventShowVideo(){
        runOnUiThread(() -> {
            Log.d(TAG, "onCreate: pos : " + MainActivity.pos);
            if (!MainActivity.cameraChanged) {
                DeepConnectingSDK.getInstance(getApplicationContext()).selectCamera(MainActivity.pos);
                MainActivity.cameraChanged = false;
            }
//
            DeepConnectingSDK.getInstance(getApplicationContext()).initVideos(svUser1, svUser2);
            svUser1.setVisibility(View.VISIBLE);
            svUser2.setVisibility(View.VISIBLE);


        });
    }


    public static void showMainProgressBar(boolean b) {
        if (b) {
            pbMain.setVisibility(View.VISIBLE);
            mActivity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            pbMain.setVisibility(View.GONE);
            mActivity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    private void setSpinner() {
        final String[] cameraPosition = getResources().getStringArray(R.array.camera);
        mArrayAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, cameraPosition);
        spinner.setAdapter(mArrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (pos != position) {
                    btnSwitch.performClick();
                    pos = position;
                    cameraChanged = !cameraChanged;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

}