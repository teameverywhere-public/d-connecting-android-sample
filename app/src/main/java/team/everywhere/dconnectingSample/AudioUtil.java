package team.everywhere.dconnectingSample;

import android.bluetooth.BluetoothA2dp;
import android.bluetooth.BluetoothHeadset;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class AudioUtil extends BroadcastReceiver {
    private static final String TAG = "AudioUtil";
    private static boolean IS_HEADSET = false;
    private static boolean IS_BLUETOOTH_CONNECTED = false;

    private BluetoothHeadset mBluetoothHeadset;

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals(Intent.ACTION_HEADSET_PLUG)) {
            int state = intent.getIntExtra("state", -1);
            switch (state) {
                case 0:
                    IS_HEADSET = false;
                    break;
                case 1:
                    IS_HEADSET = true;
                    break;
                default:
                    Log.d(TAG, "onReceive: HEADSET - No idea");

            }
        } else if (action.equals(BluetoothA2dp.ACTION_CONNECTION_STATE_CHANGED)) {
            int state = intent.getIntExtra(BluetoothA2dp.EXTRA_STATE, BluetoothA2dp.STATE_DISCONNECTED);
            if (state == BluetoothA2dp.STATE_CONNECTED) {
                Log.d(TAG, "onReceive: bluetooth is connected");
                IS_BLUETOOTH_CONNECTED = true;
            } else if (state == BluetoothA2dp.STATE_DISCONNECTED) {
                Log.d(TAG, "onReceive: bluetooth is disconnected");
                IS_BLUETOOTH_CONNECTED = false;
            }
        } else if (action.equals(BluetoothA2dp.ACTION_PLAYING_STATE_CHANGED)) {
            int state = intent.getIntExtra(BluetoothA2dp.EXTRA_STATE, BluetoothA2dp.STATE_NOT_PLAYING);
            if (state == BluetoothA2dp.STATE_PLAYING) {

            } else {

            }
        }
    }

    public static boolean isIsHeadset() {
        return IS_HEADSET;
    }

    public static boolean isIsBluetoothConnected() {
        return IS_BLUETOOTH_CONNECTED;
    }

    //    private BluetoothProfile.ServiceListener mProfileListener = new BluetoothProfile.ServiceListener()
//    {
//        @Override
//        public void onServiceConnected(int profile, BluetoothProfile proxy)
//        {
//            if (profile == BluetoothProfile.HEADSET)
//            {
//                mBluetoothHeadset = (BluetoothHeadset) proxy;
//                if(mBluetoothHeadset.getConnectedDevices().size()>0) {
//                    IS_BLUETOOTH_CONNECTED = true;
//                    Log.d(TAG,"Bluetooth device is connected");
//                }
//
//            }
//        }
//
//        @Override
//        public void onServiceDisconnected(int profile)
//        {
//            if (profile == BluetoothProfile.HEADSET)
//            {
//                mBluetoothHeadset = null;
//                IS_BLUETOOTH_CONNECTED = false;
//                Log.d(TAG,"Bluetooth device is disconnected");
//            }
//        }
//    };
//
//    public BluetoothProfile.ServiceListener getProfileListener() {
//        return mProfileListener;
//    }
}
