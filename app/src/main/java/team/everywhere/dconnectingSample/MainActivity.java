package team.everywhere.dconnectingSample;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.media.AudioManager;
//import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import androidx.core.app.ActivityCompat;
import androidx.multidex.MultiDex;

import java.util.ArrayList;

import everywhere.team.DeepConnecting.SignallingClient;

public class MainActivity extends Activity {
    private static final String TAG = "MainActivity";
    private static final int REQUEST_CAMERA_AUDIO_CODE = 1111;

    AudioUtil au;
    SignallingClient signallingClient;
    private boolean isInitialize = false;
    private BluetoothAdapter mBluetoothAdapter;
    private int audioType = 0;

    private Button btnSwitch;
    private ArrayAdapter<String> mArrayAdapter;
    public static int pos = 0;
    private String stAuthSid, stAuthToken, stId, stRoomName;
    AudioManager am;


    public static boolean cameraChanged = false;

    boolean frontCam, rearCam;
    private ArrayList<String> cameraList;
    private String[] numberOfCameras;
    private int cameraOpt;
    Spinner spinner;
    Button btnConnect;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setContentView(R.layout.activity_main);
        checkpermission();

        au = new AudioUtil();
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        spinner = (Spinner) findViewById(R.id.spinner);


//        setAudio();



        Button btnConnect =(Button) findViewById(R.id.btnConnect);
        btnConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //move to video control view
                Intent in = new Intent(MainActivity.this, VideoActivity.class);
                startActivity(in);
            }
        });

        //TODO : Remove error from camera list
        // dont' use  below code for now
        // setSpinner();
        //        getCameraList();
//        binding.setMain(this);
    }

    private void setSpinner() {
//        final String[] cameraPosition = getResources().getStringArray(R.array.camera);
        mArrayAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, cameraList);
        spinner.setAdapter(mArrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (pos != position) {

                    Log.d(TAG, "onItemSelected: pos : " + pos + ", position : " + position);
                    int selectedCamera = position;
                    if (numberOfCameras.length > 2) {
                        if (position < pos) {
                            selectedCamera = position + numberOfCameras.length;
                        }
                        cameraOpt = selectedCamera - pos;

                        Log.d(TAG, "onItemSelected: cameraOpt : " + cameraOpt);

                    } else {
                        cameraOpt = 1;
                    }

                    for (int i = 0; i < cameraOpt; i++) {
                        Log.d(TAG, "onItemSelected: perform click called");
                        btnSwitch.performClick();
                    }

                    pos = position;
                    cameraChanged = !cameraChanged;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }




    private void setAudio() {
        Log.d(TAG, "setAudio: audioType : " + audioType);
        if (am == null) {
            am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        }

        if (audioType == 2) {
            am.setMode(AudioManager.MODE_IN_COMMUNICATION);
            am.setSpeakerphoneOn(false);
        }
        if (audioType == 1) {
            am.setMode(AudioManager.MODE_IN_COMMUNICATION);
            am.setSpeakerphoneOn(false);
        }
        if (audioType == 0) {
//            am.setMode(AudioManager.MODE_IN_COMMUNICATION);
//            am.setSpeakerphoneOn(false);

            am.setMode(AudioManager.MODE_IN_CALL);
            am.setSpeakerphoneOn(true);
        }
    }

    private void checkpermission() {

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.RECORD_AUDIO
        ) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this,
                        Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{
                            Manifest.permission.RECORD_AUDIO,
                            Manifest.permission.CAMERA
                    },
                    REQUEST_CAMERA_AUDIO_CODE
            );
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        if (au != null) {
            unregisterReceiver(au);
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        IntentFilter iFilter = new IntentFilter(Intent.ACTION_HEADSET_PLUG);
        au = new AudioUtil();
        registerReceiver(au, iFilter);
        super.onResume();
    }


    public void getCameraList() {
        PackageManager pm = getPackageManager();

        Log.d(TAG, "getCameraList: front : " + pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT) + "\n" +
                "back :" + pm.hasSystemFeature(PackageManager.FEATURE_CAMERA));
        //Must have a targetSdk >= 9 defined in the AndroidManifest
        frontCam = pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT);
        rearCam = pm.hasSystemFeature(PackageManager.FEATURE_CAMERA);

        cameraList = new ArrayList<>();

        //If there are only 1 or 2 cameras :
//        if(frontCam&&rearCam){
//            cameraList.add("Front Camera");
//            cameraList.add("Back Camera");
//        } else {
//            cameraList.add("Back Camera Only");
//        }



        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            //If there are more than 2 cameras;
            CameraManager manager = null;
            manager = (CameraManager) getSystemService(CAMERA_SERVICE);
            try {
                numberOfCameras = manager.getCameraIdList();
                for (String cameraId : numberOfCameras) {
                    CameraCharacteristics chars
                            = manager.getCameraCharacteristics(cameraId);
                    Log.d(TAG, "getCameraList: cameraId : " + cameraId + " , chars : " + chars);
                    // Do something with the characteristics
                    String cameraName = "Camera " + cameraId;

                    cameraList.add(cameraName);
                }
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        }


    }
}
