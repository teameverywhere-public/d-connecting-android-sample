package team.everywhere.dconnectingSample.util;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class RxEventBus {
//    private HashMap<Object, Subject> maps = new HashMap<>();

    private RxEventBus() {
    }

    private static RxEventBus instance;

    public static RxEventBus getInstance() {
        if(instance == null){
            instance = new RxEventBus();
        }
        return instance;
    }

    private PublishSubject<Object> bus = PublishSubject.create();

//    public Observable register(Object tag, Class clazz) {
//        Subject subjects = maps.get(tag);
//        if (subjects == null) {
//            subjects = PublishSubject.create();
//            maps.put(tag, subjects);
//        }
//        return subjects;
//    }

    public void post(Object o) {
        bus.onNext(o);
    }

    public Observable<Object> toObservable() {
        return bus;
    }


//    public <T> Observable<T> register(Object tag, Class<T> clazz) {
//        Subject subjects = maps.get(tag);
//        if (subjects == null) {
//            subjects = PublishSubject.<T>create();
//            maps.put(tag, subjects);
//        }
//        return subjects;
//    }
//
//    public void post(Object o) {
//        post(o.getClass().getSimpleName(), o);
//    }
//
//    public void post(Object tag, Object o) {
//        Subject subjects = maps.get(tag);
//        if (subjects != null) {
//            subjects.onNext(o);
//        }
//    }
}
