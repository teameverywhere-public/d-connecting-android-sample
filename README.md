# D-Connecting-Android-Sample




# Import DeepConnecting SDk

## Step 1
Download DeepConnecting Module
- https://sdk.d-connecting.io/android/v2.7/DeepConnecting.aar



## Step 2.
In DeepConnecting version 2, we use manually compiled WebRTC framework.
So, please Check our sample code in gradle file.

in build.gradle(Module:app)

Add project as below 
```java
apply plugin: 'com.android.application'

android {
    compileSdkVersion 28
    defaultConfig {
        applicationId "team.everywhere.myapplication"
        minSdkVersion 23
        targetSdkVersion 28
        versionCode 1
        multiDexEnabled true
        versionName "1.0"
        testInstrumentationRunner "android.support.test.runner.AndroidJUnitRunner"


    }
    buildTypes {
        release {
            minifyEnabled false
            proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
        }
    }
    compileOptions {
        sourceCompatibility JavaVersion.VERSION_1_8
        targetCompatibility JavaVersion.VERSION_1_8
    }
}

dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar'])
    implementation 'com.android.support:appcompat-v7:28.0.0'
    implementation 'com.android.support.constraint:constraint-layout:1.1.3'
    implementation 'com.android.support:multidex:1.0.3'
    implementation 'com.android.support:support-v4:28.0.0'
    testImplementation 'junit:junit:4.12'

    implementation project(':DeepConnecting')
    implementation('io.socket:socket.io-client:0.8.3') {
        // excluding org.json which is provided by Android
        exclude group: 'org.json', module: 'json'
    }
    implementation 'com.squareup.okhttp3:okhttp:4.0.1'
    implementation 'com.squareup.okhttp3:logging-interceptor:4.0.1'
    implementation 'io.reactivex.rxjava2:rxjava:2.2.9'
    implementation 'io.reactivex.rxjava2:rxandroid:2.1.1'
    implementation 'com.appyvet:materialrangebar:1.4.3'
    implementation 'com.jakewharton.retrofit:retrofit2-rxjava2-adapter:1.0.0'
    implementation 'com.squareup.retrofit2:retrofit:2.6.1'
    implementation 'com.squareup.retrofit2:converter-jackson:2.6.1'

    //Don't use guava, it crashes WebRTC
//    implementation 'com.google.guava:guava:24.1-jre'
    implementation 'com.squareup.retrofit2:converter-gson:2.6.1'


    androidTestImplementation 'com.android.support.test:runner:1.0.2'
    androidTestImplementation 'com.google.code.findbugs:jsr305:1.3.9'
}
```

# Code Guide

### 1. Init DeepConnecting 
```java
String stAuthSid = "AuthSid from www.d-connecting.io";
String stAuthToken = "AuthToken from www.d-connecting.io";
// When Users call, caller and callee should have different stId
String stId = "User Id";
// When Users call, caller and callee should connect to same room
String stRoomName = "Room Name";
String stCountry = "SouthKorea";
//From now on, sdk distinguish countries to access closest server.
//Now we are providing two countries : SouthKorea, America(North California)
SignallingClient.instance.generateToken(
stAuthSid,
stAuthToken,
stId,
stRoomName,
stCountry
);
```
### 2. Code Structure
 - MainActivity : Call Video Activity
 - VideoActivity : Controll Video Call

### 3. Signaling server Connect 
```java
SignallingClient.getInstance(getApplicationContext()).SetTokenCallback(new SignallingClient.TokenInterface() {
    @Override
    public void onTokenGenerated() {
        DeepConnectingSDK.getInstance(getApplicationContext()).setDeepConnectingObserver(new DeepConnectingSDK.DeepConnectingObserver() {
            @Override
            public void signalServerConnencted() {
                // On Successfully Connected
                runOnUiThread(() -> {

                    int videoWidth = 640;
                    int videoHeight = 360;
                    boolean hardwareEncoding = false;
                    DeepConnectingSDK.getInstance(getApplicationContext()).setVideo(videoWidth, videoHeight, hardwareEncoding);
                    // 위코드는 실행하지 않으면 default 값으로 들어 갑니다. 640/360/true

                    Toast.makeText(VideoActivity.this, "Connected Successfully", Toast.LENGTH_SHORT).show();                    
                    // now we can call to callee.
                    btnCall.setEnabled(true);
                });
            }

            @Override
            public void connectionDisconnected() {
                Log.d(TAG, "connectionDisconnected: ");
                runOnUiThread(() ->  btnHangUp.performClick());
//

            }

            @Override
            public void connected() {
                Log.d(TAG, "connected: called");
                runOnUiThread(() -> {
                    btnHangUp.setVisibility(View.VISIBLE);
                    btnVideo.setVisibility(View.VISIBLE);

                    showMainProgressBar(false);
                    //We turned off AudioManager and mice on Test,
                    //When you use in project, turn on mice or AudioManager
                    DeepConnectingSDK.getInstance(getApplicationContext()).onToggleMic(false);
                    DeepConnectingSDK.getInstance(getApplicationContext()).stopAudioManager();

                });
            }

            @Override
            public void dataChannelOppened() {
                //datachannel opend
                runOnUiThread(() -> {
                    btnSendMessage.setVisibility(View.VISIBLE);

                });

            }

            @Override
            public void onSocketDisconnected() {
                Log.d(TAG, "onSockectDisconnected");
            }

            @Override
            public void onRoomJoined() {
                //called when user joined room by event connect;
                Log.d(TAG, "onRoomJoined: ");

            }

            @Override
            public void onHangupFinished() {
                Log.d(TAG, "onHangupFinished: ");
                //When hangup finish, close activity
                finish();
            }
            //
            @Override
            public void onMessageFromDatachannel(ByteBuffer buffer) {
                //User can get data from WebRTC datachannel 
                ByteBuffer data = buffer;
                final byte[] bytes = new byte[data.capacity()];
                data.get(bytes);
                String strData = new String(bytes, Charset.forName("UTF-8"));
                Log.d(TAG, "Got msg: " + strData);
                runOnUiThread(() -> {

                    Toast.makeText(getApplicationContext(), "Get Message : "+strData,Toast.LENGTH_LONG).show();
                });

            }


            @Override
            public void didReceiveRemoteSDP() {
                Log.d(TAG, "didReceiveRemoteSDP: called");
            }

            @Override
            public void didReceiveCandidate() {
                // On Called;
                Log.d(TAG, "didReceiveCandidate: called");
            }

            @Override
            public void didDiscoverLocalCandidate() {
                Log.d(TAG, "didDiscoverLocalCandidate: called");
            }
        });
    }
});
```
### 4. Start Call
```java
DeepConnectingSDK.getInstance(getApplicationContext()).onTryToStart();
```
### 5. Hang up
```java
DeepConnectingSDK.getInstance(getApplicationContext()).hangup();
// Below code is deprecated
// SignallingClient.getInstance(getApplicationContext()).close();
```

### 6. Capture Frame
 - You can capture frame by below code.
```java
    Button btnCapture = findViewById(R.id.btnCapture);
        btnCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //capture remote image
                svUser2.addFrameListener(new EglRenderer.FrameListener() {
                    @Override
                    public void onFrame(Bitmap bitmap) {
                        Log.d(TAG, "onFrame: captured bitmap width : "+bitmap.getWidth());
                        runOnUiThread(() -> {
                            ivCaptured.setImageBitmap(bitmap);
                            ivCaptured.setVisibility(View.VISIBLE);
                        });


                    }
                }, 1.0f);

            }
        });
```

### 7. Select Camera
By giving position, you can change camera's view(front&back). The position has to be set as *0 = Front, 1 = Back*.

This function needs to be placed at connected override function in setDeepConnectingObserver.
```java
DeepConnectingSDK.getInstance(getApplicationContext()).setDeepConnectingObserver(
new DeepConnectingSDK.DeepConnectingObserver() {
    ,,,
    @Override
    public void connected() {
        runOnUiThread(() -> {
            if (position != 0) {
                DeepConnectingSDK.getInstance(getApplicationContext()).selectCamera(position);
            }
        }
    }
```

### 8. D-Connecting Disconnect Cycle
- When remote client disconnect call, local client also leave connect room.
- Disconnect Logs will appear as below,
    - connectionDisconnected : WebRTC connection disconnected
    - onSockectDisconnected : Client left socket room (Signaling server)

